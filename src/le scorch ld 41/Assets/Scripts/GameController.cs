﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameController : MonoBehaviour
{
    public AudioClip goodEnding;
    public AudioClip okEnding;
    public AudioClip badEnding;

    public PlayerScript player;
    private static GameController _instance;
    public static GameController Instance {  get {  return _instance; } }
    public Transform curtain;

    public ReportCardScript report;

    public Transform critic;
    private CriticScript _critic;
    private float criticSoundInterval = 5;

    private AudioSource _music;
    private SoundPlayerScript _sound;
    private AudioClip[] _criticNoises;
    private Camera _camera;
    private float _originalCameraScale;
    private float _targetCameraScale;
    private static RecipeScoreResult _score;
    private bool _firstSoundPlayed;

    private float _lastCriticSound;
    private Color _fadeTarget;
    private SpriteRenderer _curtainRenderer;
    private AudioClip[] _sounds;
    private float _fadeSpeed = 0.1f;
    private bool _gameOver;


    private string[] _goodReviews = new[]
    {
        "luv it",
        "wonderful",
        "amaaazing yo"
    };

    private string[] _okReviews = new[]
    {
        "eh",
        "ok",
        "decent"
    };

    private string[] _badReviews = new[]
    {
        "piss",
        "stay away from me tiny chef",
        "so bad"
    };

    private bool _endGameReached;

    void Start()
    {
        _instance = this;
        _camera = Camera.main;
        _originalCameraScale = _camera.orthographicSize;
        _targetCameraScale = _originalCameraScale;

        _sound = gameObject.AddComponent<SoundPlayerScript>();

        _sounds = Resources.LoadAll<AudioClip>("sfx");

        _criticNoises = GetAudioClipsStartingWith("critic");

        _curtainRenderer = curtain.GetComponent<SpriteRenderer>();
        _curtainRenderer.color = Color.black;
        FadeToBlack();

        _music = GetComponent<AudioSource>();

        _critic = critic.GetComponent<CriticScript>();
        _lastCriticSound = Time.time;
    }

    public void FadeToBlack()
    {
        _fadeTarget = Color.black;
    }

    public void FadeIn()
    {
        _fadeTarget = new Color(0, 0, 0, 0);
    }

    void CriticSound()
    {
        if (!_firstSoundPlayed)
        {
            FadeIn();
            _firstSoundPlayed = true;
            _critic.StartStages();
        }

        RandomSound(critic.position, _criticNoises);
    }

    public AudioClip[] GetAudioClipsStartingWith(string s)
    {
        var result = _sounds.Where(a => a.name.StartsWith(s + "-")).ToArray();

        return result;
    }

    public void SetEndGameReached()
    {
        if (_endGameReached)
        {
            return;
        }

        _endGameReached = true;

        _critic.SetEndGameReached();

        var foods = GameObject.FindObjectsOfType<LoadableFoodScript>().Where(f => f.ingredient != Ingredients.CompletedRecipe).ToArray();
        foreach (var f in foods)
        {
            f.SetEndGameReached();
        }
    }

    public void RandomSound(Vector3 position, AudioClip[] clips)
    {
        var sound = Easy.Random(clips);

        if (sound == null)
        {
            return;
        }

        _sound.Play(position, sound, 1);
    }

    public void SetPowerScale(float scale)
    {
        ZoomCameraOut(scale);
    }

    public void ClearPowerScale()
    {
        ZoomCameraOut(0);
    }

    public void ZoomCameraOut(float scale)
    {
        _targetCameraScale = _originalCameraScale + _originalCameraScale * scale;
    }

    void Update()
    {
        var scaled = Mathf.Lerp(_camera.orthographicSize, _targetCameraScale, Time.deltaTime);
        _camera.orthographicSize = scaled;

        if (_curtainRenderer.color != _fadeTarget)
        {
            _curtainRenderer.color = Color.Lerp(_curtainRenderer.color, _fadeTarget, _fadeSpeed);
        }

        if (_gameOver)
        {
            return;
        }

        if (Time.time - _lastCriticSound >= criticSoundInterval && Easy.Chance(0.1f))
        {
            Debug.Log(Time.time);
            _lastCriticSound = Time.time;
            CriticSound();
        }
    }

    public static void ReportRecipeScore(RecipeScoreResult score)
    {
        _score = score;
    }

    public void EndGame()
    {
        _gameOver = true;

        player.GameOver();

        _music.Stop();
        Physics2D.gravity = Physics2D.gravity * 0.1f;
        var cameraControl = Camera.main.transform.GetComponent<CameraFollowScript>();
        cameraControl.target = critic;
        ClearPowerScale();

        _fadeSpeed = 0.05f;

        FadeToBlack();

        var grade = new[] {_score.RightSequence, _score.AllIngredients, !_score.ExtraIngredients}.Count(s => s);
        var index = grade - 1;
        var endSounds = new[] {badEnding, okEnding, goodEnding};
        var sound = endSounds[index];

        StartCoroutine(CoRo.WaitThen(3, () =>
        {
            _sound.Play(critic.position, sound, 1);

            StartCoroutine(CoRo.WaitThen(3, () =>
            {
                ShowReportCard(grade, index, cameraControl);
            }));
        }));
    }

    public void SetCameraZShake(float z)
    {
    }

    private void ShowReportCard(int grade, int index, CameraFollowScript cameraControl)
    {
        cameraControl.Disable();
        // cameraControl.offset = new Vector3(0, 0, 10);
        // cameraControl.target = report.transform;

        Camera.main.transform.position = report.transform.position - new Vector3(0, 0, 1);
        Camera.main.backgroundColor = Color.black;

        _music.Play();

        var reviewChoices = new[] {_badReviews, _okReviews, _goodReviews};

        var category = reviewChoices[index];

        var review = Easy.Random(category);

        ZoomCameraOut(0.25f);
        report.Show(review);
    }
}
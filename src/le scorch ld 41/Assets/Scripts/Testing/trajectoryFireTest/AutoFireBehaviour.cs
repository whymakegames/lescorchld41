﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AutoFireBehaviour : MonoBehaviour
{
    private CannonScript _cannon;
    public float interval = 0.5f;
    private Timer _fire;

	// Use this for initialization
	void Start ()
	{
	    _cannon = GetComponentInChildren<CannonScript>();
	    _fire = new Timer(interval);
	}
	
	// Update is called once per frame
	void Update ()
	{
	    _cannon.AimUp();

	    if (_fire.TimeUp(Time.deltaTime))
	    {
	        _cannon.Fire();
	        _fire.Reset();
	    }
	}
}

﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class AutoReloadScene : MonoBehaviour
{
    public float interval;

    void Start()
    {
        StartCoroutine(CoRo.WaitThen(interval, () => SceneManager.LoadScene(SceneManager.GetActiveScene().name)));
    }
}
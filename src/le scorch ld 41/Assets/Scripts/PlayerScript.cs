﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerScript : MonoBehaviour
{
    public AudioClip driveTank;
    private MovementControllerScript _movement;
    public Transform cannonObject;
    public Transform catapultObject;
    private CatapultScript _catapult;
    private CannonScript _cannon;
    private GroundCheckerScript _grounding;
    private bool _firing;
    private LoadableFoodScript _loadNext;
    private AudioSource _tankDrive;
    private bool _gameOver;

    // Use this for initialization
    void Start ()
    {
        _tankDrive = gameObject.AddComponent<AudioSource>();
        _tankDrive.clip = driveTank;

	    _movement = GetComponent<MovementControllerScript>();
	    _cannon = cannonObject.GetComponent<CannonScript>();
	    _catapult = catapultObject.GetComponent<CatapultScript>();
	    _grounding = GetComponent<GroundCheckerScript>();
	}

	// Update is called once per frame
	void Update ()
	{
	    if (Input.GetKey(KeyCode.Escape))
	    {
	        Application.Quit();
	    }

	    if (_gameOver)
	    {
	        return;
	    }

	    if (_loadNext != null)
	    {
	        _catapult.Load(_loadNext);
	        _loadNext = null;
	    }

	    var left = Inputs.GetLeftStickInput();

	    if (_movement.SetInput(left.x, 0))
	    {
	        if (!_tankDrive.isPlaying)
	        {
	            _tankDrive.Play();
            }
	    }
	    else
	    {
	        _tankDrive.Stop();
	    }

	    if (TryUseCatapult(left) || TryUseCannon(left))
	    {
	        _movement.SetInput(0, 0);
	        _firing = true;
	        _movement.Freeze(false);
	        _grounding.Anchor();
	        return;
	    }
	    else
	    {
	        _firing = false;
	        GameController.Instance.ClearPowerScale();
            _grounding.BreakAnchor();
            _movement.ClearFreeze();
	    }

        if (Inputs.JumpDown())
	    {
	        _movement.SetJumpDown();
	    }
        else if (Inputs.JumpHeld())
	    {
	        _movement.SetJumpHeld();
	    }
	    else
	    {
	        _movement.SetJumpReleased();
	    }
	}

    bool TryUseCatapult(Vector2 input)
    {
        if (_catapult.HasFood)
        {
            _movement.Freeze(false);
            _movement.SetJumpReleased();

            if (_catapult.LoadComplete())
            {
                if (Inputs.FirePressed())
                {
                    _catapult.UpdateAim(input);
                }
                else if (_catapult.StartedAiming())
                {
                    _catapult.Fire();
                }
            }

            return true;
        }

        return false;
    }

    private bool TryUseCannon(Vector2 left)
    {
        if (Inputs.FirePressed())
        {
            _movement.Freeze(false);
            _cannon.UpdateAim(left);

            return true;
        }
        else if (_cannon.StartedAiming())
        {
            _cannon.Fire();

            return true;
        }

        return false;
    }

    void OnCollisionEnter2D(Collision2D coll)
    {
        if (!_catapult.HasFood)
        {
            var food = coll.transform.GetComponent<LoadableFoodScript>();
            if (food != null)
            {
                _catapult.Load(food);
            }
        }
    }

    public void TryLoadFood(LoadableFoodScript completed)
    {
        if (!_firing)
        {
            _loadNext = completed;
        }
    }

    public void GameOver()
    {
        _gameOver = true;
        _movement.Freeze(true);
    }
}

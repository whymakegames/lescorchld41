﻿using UnityEngine;

public class TimeSpawner : MonoBehaviour
{
    public Transform[] spawns;
    public float interval;
    public bool noLoop;
    private int _next;

    void Start()
    {
        Schedule();
    }

    void Schedule()
    {
        StartCoroutine(CoRo.WaitThen(interval, SpawnNext));
    }

    void SpawnNext()
    {
        Instantiate(spawns[_next], transform.position, transform.rotation);

        _next++;

        if (_next >= spawns.Length)
        {
            if (noLoop)
            {
                return;
            }

            _next = 0;
        }

        Schedule();
    }
}
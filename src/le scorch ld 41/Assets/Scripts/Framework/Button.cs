﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public static class CoRo
{
    public static IEnumerator WaitThen(float seconds, params Action[] things)
    {
        yield return new WaitForSeconds(seconds);
        foreach (var a in things)
        {
            a();
        }
    }
}

public class Button
{
    private bool _firstPress;
    private bool _held;

    public void FirstPress()
    {
        _firstPress = true;
        _held = true;
    }

    public void Hold()
    {
        _firstPress = false;
        _held = true;
    }

    public void Release()
    {
        _firstPress = false;
        _held = false;
    }

    public bool IsFirstPress()
    {
        return _firstPress;
    }

    public bool IsHeld()
    {
        return _held;
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

/// <summary>
/// utility functions to do complicated stuff I DONT KNOW HOW TO MAKE GAMES
/// </summary>
public static class Tuffy
{
    public static int GetNegPos(float f, float threshold = 0)
    {
        if (f < (0 - threshold))
        {
            return -1;
        }

        if (f > (0 + threshold))
        {
            return 1;
        }

        return 0;
    }


    public static void ChangeColor(Transform transform, Color target, float time)
    {
        var renderer = transform.GetComponent<SpriteRenderer>();
        if (renderer != null)
        {
            var lerped = Color.Lerp(renderer.color, target, time);
            renderer.color = lerped;
        }
    }

    public static void ShakeRotate(Transform transform, float time, float speed, float amount)
    {
        var shake = Mathf.Sin(time * speed) * amount;
        transform.Rotate(0, 0, shake);
    }

    public static void LookAt2dRight(Vector3 target, Transform transform)
    {
        transform.right = target - transform.position;
    }

    public static void LookAt2dForward(Vector3 target, Transform transform)
    {
        transform.forward = target - transform.position;
    }

    public static Vector3 MultiplyVectorX(Vector3 vector, int b)
    {
        return new Vector3(vector.x * b, vector.y, vector.z);
    }

    public static float AddClamp(float value, float add, float max)
    {
        var updated = value + add;

        if (updated > max)
        {
            return max;
        }

        return updated;
    }

    public static float SubClamp(float value, float sub, float min)
    {
        var updated = value - sub;

        if (updated < min)
        {
            return min;
        }

        return updated;
    }

    public static void ClampZEuler(Transform target, float thresh)
    {
        var degrees = target.localEulerAngles.z;
        if (degrees > 180)
        {
            degrees -= 360;
        }


        var absDegrees = Mathf.Abs(degrees);

        if (absDegrees >= Mathf.Abs(thresh))
        {
            var dir = Tuffy.GetNegPos(degrees);
            if (dir != 0)
            {
                if (thresh < 0)
                {
                    thresh += 360;
                }

                //Debug.Log(thresh);

                target.localEulerAngles = new Vector3(target.localEulerAngles.x, target.localEulerAngles.y, thresh);
            }
        }
    }
}
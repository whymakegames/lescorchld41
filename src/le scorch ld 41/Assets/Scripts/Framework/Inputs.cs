﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public static class Inputs
{
    public static Vector2 GetLeftStickInput()
    {
        return new Vector2(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"));
    }

    public static bool JumpDown()
    {
        return Input.GetButtonDown("Fire1");
    }

    public static bool JumpHeld()
    {
        return Input.GetButton("Fire1");
    }

    public static bool JumpUp()
    {
        return Input.GetButtonUp("Fire1");
    }

    public static Vector2 GetRightStickInput()
    {
        return new Vector2(Input.GetAxis("RightStickX"), Input.GetAxis("RightStickY"));
    }

    public static bool Fire()
    {
        return Input.GetButtonDown("Fire2");
    }

    public static bool FirePressed()
    {
        return Input.GetButton("Fire2");
    }
}
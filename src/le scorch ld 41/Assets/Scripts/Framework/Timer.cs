﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

public class Timer
{
    private float _elapsed;
    private readonly float _length;

    public Timer(float length)
    {
        _length = length;
        _elapsed = length;
    }

    public float GetPercentElapsed()
    {
        return _elapsed / _length;
    }

    public bool TimeUp(float a)
    {
        if (_elapsed >= _length)
        {
            return true;
        }

        _elapsed += a;

        return false;
    }

    public void Reset()
    {
        _elapsed = 0;
    }
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ZRotationLimitScript : MonoBehaviour
{
    public Transform target;
    public float rotationLimit;

    void Start()
    {
    }

    void Update()
    {
        Tuffy.ClampZEuler(target, rotationLimit);
    }
}
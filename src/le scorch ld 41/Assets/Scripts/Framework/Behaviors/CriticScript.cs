﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public class CriticScript : MonoBehaviour
{
    private Animator _animator;
    private int _stage;
    float stageLength = 60;
    int totalStages = 3;
    private Timer _timer;
    private bool _endGame;
    private bool _stagesStarted;

    void Start()
    {
        _animator = GetComponent<Animator>();
        _timer = new Timer(stageLength);
        _timer.Reset();
        _stage = 0;
    }

    void OnCollisionEnter2D(Collision2D coll)
    {
        var food = coll.gameObject.GetComponent<LoadableFoodScript>();
        if (food != null && food.ingredient == Ingredients.CompletedRecipe)
        {
            GameController.Instance.EndGame();
        }
    }

    public int GetStageModifier()
    {
        return _stage;
    }

    public void SetEndGameReached()
    {
        _animator.SetBool("Hungry", true);
        _endGame = true;
    }

    public void StartStages()
    {
        _stagesStarted = true;
    }

    void Update()
    {
        if (!_stagesStarted)
        {
            return;
        }

        if (_endGame)
        {
            return;
        }

        if (_timer.TimeUp(Time.deltaTime) && _stage < totalStages - 1)
        {
            _timer.Reset();
            _stage++;
            Debug.Log("Stage " + _stage);

            _animator.SetInteger("Stage", _stage);
        }
    }
}
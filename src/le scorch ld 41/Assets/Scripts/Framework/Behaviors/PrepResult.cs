﻿using UnityEngine;

public class PrepResult : MonoBehaviour
{
    public Ingredients input;
    public Transform outputPrototype;
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ElevatorScript : MonoBehaviour
{
    public enum ElevatorType
    {
        Step,
        FullPath
    }

    public enum ActivationMode
    {
        Touch,
        AlwaysActive
    }

    public Transform pathContainer;
    public ElevatorType type;
    public ActivationMode activation;
    public int nextStep;
    public float activationCooldown = 5;
    public float stepTime = 0.5f;
    public bool noShootToActivate;

    private Transform[] _path;

    private AwareAnchorScript _anchorAwareness;

    private float leftOccupiedCheck = 0.3f;
    private float rightOccupiedCheck = 0.7f;
    private Collider2D _collider;
    private bool _activated;
    private Transform _startPoint;
    private bool _canActivate;
    private Timer _stepTimer;

    // Use this for initialization
	void Start ()
	{
	    _stepTimer = new Timer(stepTime);
	    _stepTimer.Reset();

        SetCanActivate(true);
	    _collider = GetComponent<Collider2D>();

	    _path = new Transform[pathContainer.childCount];
	    for (var i = 0; i < pathContainer.childCount; i++)
	    {
	        var child = pathContainer.GetChild(i);
	        _path[i] = child;
	    }

	    _anchorAwareness = GetComponent<AwareAnchorScript>();
	}
	
	// Update is called once per frame
	void Update () {
	    if (_canActivate && activation == ActivationMode.AlwaysActive)
	    {
            Debug.Log("Auto activated!");
	        Activate();
	    }

	    if (_activated)
	    {
	        _stepTimer.TimeUp(Time.deltaTime);

	        var target = _path[nextStep];
	        var nextPosition = Vector2.Lerp((Vector2)transform.position, (Vector2)target.transform.position, _stepTimer.GetPercentElapsed());
	        transform.position = nextPosition;

	        if (Mathf.Abs(Vector2.Distance(nextPosition, target.position)) <= 0.1f)
	        {
	            _stepTimer.Reset();
                Debug.Log("hit the spot");
	            nextStep++;

	            if (type == ElevatorType.Step)
	            {
	                PathComplete();
	            }

	            if (nextStep >= _path.Length)
	            {
	                if (type == ElevatorType.FullPath)
	                {
                        PathComplete();
	                }

	                nextStep = 0;
	            }
            }
	    }
	}

    void PathComplete()
    {
        _activated = false;
        StartCoroutine(CoRo.WaitThen(activationCooldown, () => SetCanActivate(true)));
    }

    public void Deactivate()
    {
        PathComplete();
    }

    public void Activate()
    {
        SetCanActivate(false);
        _activated = true;
    }

    private void SetCanActivate(bool canActivate)
    {
        Debug.Log("Can activate!");
        _canActivate = canActivate;
    }

    void OnCollisionEnter2D(Collision2D coll)
    {
        if (coll.gameObject.tag == Tags.Projectile)
        {
            if (_anchorAwareness != null) 
            {
                
                var anchor = _anchorAwareness.GetAnchoredObject();
                if (anchor != null)
                {
                    Debug.Log("Shot anchor aware thing");
                    var move = anchor.gameObject.GetComponent<MovementControllerScript>();
                    if (move != null)
                    {
                        move.ForwardPush();
                    }
                }
            }

            if (noShootToActivate)
            {
                return;
            }
        }

        if (activation == ActivationMode.Touch && _canActivate && PlatformOccupied())
        {
            Activate();
        }
    }

    private bool PlatformOccupied()
    {
        var bounds = _collider.bounds.max.x;
        var left = bounds * leftOccupiedCheck;
        var right = bounds * rightOccupiedCheck;

        foreach (var p in new[] {left, right})
        {
            var origin = new Vector2(p, _collider.bounds.min.y);
            var cast = Physics2D.Raycast(origin, Vector2.up, 1);

            Debug.DrawLine(origin, (Vector3)origin + transform.up * 3, Color.magenta, 2);

            if (cast.collider != null)
            {
                return true;
            }
        }

        return false;
    }
}

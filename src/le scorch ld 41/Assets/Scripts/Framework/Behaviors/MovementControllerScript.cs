﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;


public class MovementControllerScript : MonoBehaviour
{
    private Rigidbody2D _body;
    private Animator _animator;
    public float moveSpeed = 10;
    public float jumpAcceleration = 4;
    public float jumpMaxSpeed = 30;
    public float jumpLength = 0.2f;
    private int _lastDir = 0;
    private float _originalLocalScaleX;
    private Timer _jumpState;
    private Timer _jumpCooldown;
    private float _yForce;
    public float maxHoverHeight = 5;
    private bool _jumping;
    private GroundCheckerScript _grounder;
    private Vector2 _input;
    private Button _jump;
    private bool _frozen;
    public bool noAutoUpdateRotation = false;
    private bool _impulseNextUpdate = false;
    private bool _animatorHasMoving;
    private float _originalGravity;

    // Use this for initialization
    void Start()
    {
        _jump = new Button();
        _body = GetComponent<Rigidbody2D>();
        _animator = GetComponent<Animator>();
        _grounder = GetComponent<GroundCheckerScript>();
        _originalLocalScaleX = transform.localScale.x;
        _jumpState = new Timer(jumpLength);
        _jumpCooldown = new Timer(0.1f);
        _originalGravity = _body.gravityScale;

        if (_animator != null)
        {
            _animatorHasMoving = _animator.parameters.Any(p => p.name == "Moving");
        }
    }

    void UpdateDirection(int dir)
    {
        if (_lastDir != dir && dir != 0)
        {
            _lastDir = dir;

            if (noAutoUpdateRotation)
            {
                return;
            }

            var rot = transform.rotation.y;
            if (dir == 1)
            {
                rot = 0;
            }
            else if (dir == -1)
            {
                rot = -180f;
            }

            transform.rotation = new Quaternion(0, rot, 0, 0);
            // transform.localScale = new Vector3(_originalLocalScaleX * dir, transform.localScale.y, transform.localScale.z);
        }
    }

    public int GetDirection()
    {
        return _lastDir;
    }

    public void SetJumpDown()
    {
        _jump.FirstPress();
    }

    public void SetJumpHeld()
    {
        _jump.Hold();
    }

    public void SetJumpReleased()
    {
        _jump.Release();
    }

    public bool SetInput(float x, float y, float duration = 0)
    {
        _input = new Vector2(x, y);

        if (duration > 0)
        {
            StartCoroutine(AutoClearInput(duration));
        }

        var dir = Tuffy.GetNegPos(_input.x, 0.1f);

        UpdateDirection(dir);

        return dir != 0;
    }

    IEnumerator AutoClearInput(float duration)
    {
        yield return new WaitForSeconds(duration);
        _input = new Vector2(0, 0);
    }

    void FixedUpdate()
    {
        if (_frozen)
        {
            return;
        }

        if (_impulseNextUpdate)
        {
            var force = transform.right * moveSpeed;
            _body.AddForce(force, ForceMode2D.Impulse);
            _impulseNextUpdate = false;
        }

        // if we are allowed to jump based on cooldown
        if (_grounder.IsGrounded() && _jumpCooldown.TimeUp(Time.deltaTime))
        {
            // start a jump if the jump button has just been pressed
            if (_jump.IsFirstPress())
            {
                var jumpForce = transform.up * jumpAcceleration;
                _body.AddForce(jumpForce, ForceMode2D.Impulse);

                _jumpCooldown.Reset();
            }
        }

        if (_lastDir != 0 && _input.x != 0)
        {
            _body.AddForce(transform.right * moveSpeed);
        }

        var upDownDir = Tuffy.GetNegPos(_input.y, 0.1f);
        if (upDownDir != 0)
        {
            _body.AddForce(transform.up * moveSpeed * upDownDir);
        }

        if (_animator != null && _animatorHasMoving)
        {
            _animator.SetBool("Moving", _body.velocity.x != 0);
        }
    }

    public void ForwardPush()
    {
        _impulseNextUpdate = true;
    }

    public void Freeze(bool hard)
    {
        if (hard)
        {
            _body.velocity = new Vector2(0, 0);
            _body.gravityScale = 0;
        }
        
        _frozen = true;
    }

    public void ClearFreeze()
    {
        _frozen = false;
        _body.gravityScale = _originalGravity;
    }

    public void ForceToward(Vector3 position)
    {
        ForceToward(position, 1);
    }

    public void ForceToward(Vector3 position, float power)
    {
        Tuffy.LookAt2dRight(position, transform);
        _body.AddForce(position - transform.position * power);
    }
}

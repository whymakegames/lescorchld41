﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollowScript : MonoBehaviour
{
    public Transform target;
    public Vector3 offset;
    private bool _enabled;

    void Start()
    {
        offset = Camera.main.transform.position - target.position;
        _enabled = true;
    }

    void Update()
    {
        if (!_enabled)
        {
            return;
        }

        var lerped = Vector3.Lerp(Camera.main.transform.position, target.position + offset, Time.deltaTime);
        Camera.main.transform.position = new Vector3(lerped.x, lerped.y, Camera.main.transform.position.z);
    }

    public void Disable()
    {
        _enabled = false;
    }
}
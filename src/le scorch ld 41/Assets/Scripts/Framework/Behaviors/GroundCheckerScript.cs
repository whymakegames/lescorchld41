﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GroundCheckerScript : MonoBehaviour
{
    public bool anchoringEnabled;
    bool _anchored;

    private Vector3? _anchorOffset;
    private Collider2D _collider;
    private Animator _animator;
    private bool _grounded;
    public LayerMask ground;
    public float groundedDistance = 0.1f;
    private RaycastHit2D? _grounder;
    private AwareAnchorScript _anchorAware;

    // Use this for initialization
    void Start()
    {
    }

    Collider2D TryGetCollider()
    {
        return _collider = _collider ?? GetComponent<Collider2D>();
    }

    void InitializeAnchor()
    {
        Debug.Log("Anchored " + transform.name + "!");
        _anchored = true;

        if (_anchorOffset == null)
        {
            _anchorOffset = transform.position - _grounder.Value.transform.position;
            var anchorAware = _grounder.Value.transform.gameObject.GetComponent<AwareAnchorScript>();
            if (anchorAware != null)
            {
                anchorAware.NotifyAnchor(this);
                _anchorAware = anchorAware;
            }

            _anchored = true;
        }
    }

    void Update()
    {
        if (!_anchored && anchoringEnabled && IsGrounded() && _grounder != null)
        {
            InitializeAnchor();
        }

        if (_anchored)
        {
            if (_grounder == null || _anchorOffset == null)
            {
                BreakAnchor();
                return;
            }

            transform.position = _grounder.Value.transform.position + _anchorOffset.Value;
        }
        else
        {
            _anchored = false;
            
            if (_anchorAware != null)
            {
                _anchorAware.NotifyAnchorBroken();
                _anchorAware = null;
            }
            _anchorOffset = null;
        }
    }

    public void BreakAnchor()
    {
        _anchorOffset = null;
        _anchored = false;
    }

    public void Anchor()
    {
        _anchored = true;
    }

    public bool IsGrounded()
    {
        return _grounded;
    }

    bool CheckGrounded()
    {
        return GetGroundDistance(groundedDistance) != null;
    }

    RaycastHit2D? GetGrounder(float range)
    {
        var collider = TryGetCollider();
        if (collider == null)
        {
            return null;
        }

        var sources = new[]
        {
            collider.bounds.min,
            new Vector3(collider.bounds.max.x, collider.bounds.min.y, collider.bounds.min.z),
            new Vector3(collider.bounds.max.x, collider.bounds.min.y, collider.bounds.min.z),
            new Vector3(collider.bounds.center.x, collider.bounds.min.y, collider.bounds.min.z)
        };
        foreach (var source in sources)
        {
            var cast = Physics2D.Raycast(source, Vector2.down, range, ground);

            if (cast.collider != null)
            {
                _grounder = cast;
                return cast;
            }
            else
            {
                _grounder = null;
                Debug.DrawLine(source, new Vector3(source.x, source.y - range, source.z), Color.red);
            }
        }

        return null;
    }

    public float? GetGroundDistance(float range)
    {
        var grounder = GetGrounder(range);

        if (grounder != null)
        {
            return grounder.Value.distance;
        }

        return null;
    }

    void FixedUpdate()
    {
        _grounded = CheckGrounded();
    }
}

﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

[ExecuteInEditMode]
public class LoadableFoodScript : MonoBehaviour
{
    class ChildFood
    {
        public LoadableFoodScript food;
        public Vector3 offset;
    }

    public AudioClip pickupSound;
    private SoundPlayerScript _sound;
    public LayerMask firedLayer;
    public Ingredients ingredient;
    public Sprite sprite;
    private SpriteRenderer _renderer;
    private Rigidbody2D _body;
    private MovementControllerScript _move;
    private bool _wasFired;
    private GroundCheckerScript _grounder;
    private int _defaultLayer;
    private CircleCollider2D _collider;
    private bool _fading;
    private List<ChildFood> _childFoods;
    private AudioClip[] _squishes;

    // Use this for initialization
    void Start()
    {
        if (Application.isPlaying)
        {
            _collider = gameObject.AddComponent<CircleCollider2D>();
            _collider.radius = _collider.radius * 0.5f;
        }

        _grounder = GetComponent<GroundCheckerScript>();
        _renderer = GetComponent<SpriteRenderer>();
        _renderer.sprite = sprite;

        if (_fading)
        {
            _renderer.color = new Color(1, 1, 1, 0);
        }

        _body = GetComponent<Rigidbody2D>();
        _move = GetComponent<MovementControllerScript>();
        _defaultLayer = gameObject.layer;

        _sound = SoundPlayerScript.Initialize(this);
    }

    public bool WasFired()
    {
        return _wasFired;
    }

    void Update()
    {
        _squishes = _squishes ?? GameController.Instance.GetAudioClipsStartingWith("squish");

        if (_grounder != null && _grounder.IsGrounded())
        {
            _wasFired = false;
            gameObject.layer = _defaultLayer;
        }

        if (_childFoods != null && _childFoods.Count > 0)
        {
            for (int i = 0; i < _childFoods.Count; i++)
            {
                var f = _childFoods[i];
                f.food.transform.position = transform.position + f.offset;
            }
        }

        if (_fading)
        {
            Tuffy.ChangeColor(transform, Color.white, Time.deltaTime * 14);
            if (_renderer.color == Color.white)
            {
                _fading = false;
            }
        }
    }

    void OnGUI()
    {
        UpdateSprite();
    }

    void UpdateSprite()
    {
        _renderer = GetComponent<SpriteRenderer>();
        _renderer.sprite = sprite;
    }

    public void SetIsLoaded()
    {
        _grounder.BreakAnchor();
        _collider.enabled = false;
        _move.Freeze(true);

        MakeNoise();
    }

    void MakeNoise()
    {
        if (pickupSound != null)
        {
            _sound.Play(pickupSound);
        }
        else
        {
            GameController.Instance.RandomSound(transform.position, _squishes);
        }
    }

    public void Release(float speed)
    {
        _wasFired = true;
        _collider.enabled = true;

        gameObject.layer = 10;

        _move.moveSpeed = speed;
        _move.ClearFreeze();
        _move.ForwardPush();
    }

    public void SetProcessing()
    {
        MakeNoise();
        _move.Freeze(true);
    }

    public void FadeIn()
    {
        _fading = true;
    }

    public void SetMealPrepping()
    {
        _collider.enabled = false;
        _move.Freeze(true);
    }

    public void AddChild(LoadableFoodScript food)
    {
        _childFoods = _childFoods ?? new List<ChildFood>();

        var child = new ChildFood
        {
            food = food,
            offset = food.transform.position - transform.position
        };

        _childFoods.Add(child);
    }

    public void SetEndGameReached()
    {
        _renderer.color = new Color(1, 1, 1, 0.5f);
        _collider.enabled = false;
        _move.Freeze(true);
    }
}

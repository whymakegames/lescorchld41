﻿using UnityEngine;

public class MealRecipePart : MonoBehaviour
{
    public Ingredients ingredient;
    public int[] validPositions;
}
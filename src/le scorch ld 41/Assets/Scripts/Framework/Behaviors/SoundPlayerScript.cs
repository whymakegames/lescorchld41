﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public class SoundPlayerScript : MonoBehaviour
{
    public void Play(AudioClip clip)
    {
        AudioSource.PlayClipAtPoint(clip, transform.position);
    }

    public void Play(AudioClip clip, float volume)
    {
        AudioSource.PlayClipAtPoint(clip, transform.position, volume);
    }

    public void Play(Vector3 position, AudioClip clip, float volume)
    {
        AudioSource.PlayClipAtPoint(clip, position, volume);
    }

    public static SoundPlayerScript Initialize(MonoBehaviour playerScript)
    {
        return playerScript.gameObject.AddComponent<SoundPlayerScript>();
    }
}
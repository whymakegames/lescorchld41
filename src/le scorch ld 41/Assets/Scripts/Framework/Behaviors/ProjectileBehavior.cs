﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectileBehavior : MonoBehaviour
{
    private Animator _animator;
    private MovementControllerScript _movement;
    private Rigidbody2D _body;

    // Use this for initialization
    void Start()
    {
        _animator = GetComponent<Animator>();
        _movement = GetComponent<MovementControllerScript>();
        _body = GetComponent<Rigidbody2D>();
    }

    public void Impact()
    {
        // freeze rotation
        _body.freezeRotation = true;
        _body.gravityScale = 0;

        _animator.SetBool("Impact", true);
        _movement.Freeze(true);
    }

    void OnCollisionEnter2D(Collision2D coll)
    {
        Impact();
    }
}
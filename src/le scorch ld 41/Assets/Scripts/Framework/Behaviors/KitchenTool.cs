﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class KitchenTool : MonoBehaviour
{
    public AudioClip activateSound;
    public Transform prepRejectionTarget;
    public Transform prepStartAnchor;
    public Transform prepCompleteExit;
    public Transform prepCompleteExitAimPoint;

    private SoundPlayerScript _sound;
    float _prepDuration = 1;
    private LoadableFoodScript _currentPreparingFood;
    private Dictionary<Ingredients, Transform> _preparationMap;
    private Transform _prepResult;

    void Start()
    {
        _sound = SoundPlayerScript.Initialize(this);
        var preparationMaps = GetComponentsInChildren<PrepResult>();
        _preparationMap = preparationMaps.ToDictionary(p => p.input, p => p.outputPrototype);
    }

    void Prepare(LoadableFoodScript food, Transform prepResult)
    {
        if (activateSound != null)
        {
            _sound.Play(activateSound);
        }

        _currentPreparingFood = food;
        _prepResult = prepResult;
        food.SetProcessing();

        StartCoroutine(CoRo.WaitThen(_prepDuration, EndPrepareFood));
    }

    void EndPrepareFood()
    {
        Debug.Log("Food prepped!");

        Destroy(_currentPreparingFood.gameObject);
        var prepped = Instantiate(_prepResult, prepCompleteExit.position, transform.rotation);
        var food = prepped.GetComponent<LoadableFoodScript>();
        food.FadeIn();

        _currentPreparingFood = null;
    }

    void Update()
    {
        if (_currentPreparingFood != null)
        {
            Debug.Log("preparing!");
            _currentPreparingFood.transform.position = Vector3.Lerp(_currentPreparingFood.transform.position, prepStartAnchor.transform.position, Time.deltaTime);
            Tuffy.ShakeRotate(_currentPreparingFood.transform, Time.time, 40, 3);
            Tuffy.ChangeColor(_currentPreparingFood.transform, new Color(1, 0, 0, 0), Time.deltaTime * 6);
        }
    }


    void OnCollisionEnter2D(Collision2D coll)
    {
        var food = coll.gameObject.GetComponent<LoadableFoodScript>();
        if (food != null)
        {
            Transform prepResult;
            if (_currentPreparingFood == null && _preparationMap.TryGetValue(food.ingredient, out prepResult))
            {
                Prepare(food, prepResult);
            }
            else
            {
                var move = food.GetComponent<MovementControllerScript>();
                if (move != null)
                {
                    move.ForceToward(prepRejectionTarget.position, 350);
                }
            }
        }
    }
}
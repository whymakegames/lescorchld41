﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CannonScript : MonoBehaviour
{
    public Transform projectileProto;
    public LayerMask ground;
    public float groundedDistance = 0.1f;
    public Transform firePoint;
    public float cooldown = 1;
    public Transform aimIndicator;
    public float upAimThreshold = 45;
    public float downAimThreshold = -45;
    private float _chargeTime = 5;
    float _aimDegrees = 1;

    private Animator _animator;
    private bool _grounded;
    private Timer _cannonCooldown;
    private bool _canFire;
    private bool _aimingNow;
    private Transform _activeAimIndicator;
    private Timer _fullPower;
    private float _firePowerScale;

    public CannonScript()
    {
    }

    // Use this for initialization
    void Start()
    {
        _animator = GetComponent<Animator>();
        _cannonCooldown = new Timer(cooldown);

        _fullPower = new Timer(_chargeTime);
        _fullPower.Reset();

        StopAiming();
    }

    void Update()
    {
        if (_cannonCooldown.TimeUp(Time.deltaTime))
        {
            _canFire = true;
        }

        if (_aimingNow)
        {
            var percent = _fullPower.GetPercentElapsed();

            GameController.Instance.SetPowerScale(percent);

            _firePowerScale = percent;
            _fullPower.TimeUp(Time.deltaTime);
            _activeAimIndicator.localScale = new Vector3(3, 3, 1) * percent;
        }
        
    }

    Transform InstantiateAtFirePoint(Transform proto, int scale)
    {
        var result = Instantiate(proto, firePoint.position, transform.rotation);

        result.localScale = new Vector3(scale, scale, 1);

        return result;
    }

    public void Fire()
    {
        _fullPower.Reset();
        _cannonCooldown.Reset();
        _canFire = false;

        var projectile = InstantiateAtFirePoint(projectileProto, 1);

        var move = projectile.GetComponent<MovementControllerScript>();

        projectile.gameObject.SetActive(true);

        move.moveSpeed *= _firePowerScale;
        move.ForwardPush();
        move.noAutoUpdateRotation = true;

        StopAiming();
    }

    void StopAiming()
    {
        SetAiming(false);
    }

    void StartAiming()
    {
        SetAiming(true);
    }

    void SetAiming(bool aiming)
    {
        if (_aimingNow == aiming)
        {
            return;
        }

        _aimingNow = aiming;

        if (aiming)
        {
            var indicator = InstantiateAtFirePoint(aimIndicator, 0);
            _activeAimIndicator = indicator;
        }
        else if (_activeAimIndicator != null)
        {
            Destroy(_activeAimIndicator.gameObject);
            _activeAimIndicator = null;
        }

    }

    public void AimUp()
    {
        transform.Rotate(Vector3.forward, _aimDegrees);
        ClampZ(upAimThreshold);
    }

    public void AimDown()
    {
        transform.Rotate(Vector3.back, _aimDegrees);
        ClampZ(downAimThreshold);
    }

    void ClampZ(float aimThreshold)
    {
        Tuffy.ClampZEuler(transform.transform, aimThreshold);
    }

    public void UpdateAim(Vector2 input)
    {
        StartAiming();

        var offset = firePoint.transform.right * 2;
        _activeAimIndicator.transform.position = firePoint.transform.position + offset;

        _activeAimIndicator.transform.rotation = transform.rotation;

        var dir = Tuffy.GetNegPos(input.y, 0.1f);

        if (dir != 0)
        {
            if (dir == 1)
            {
                AimUp();
            }
            else
            {
                AimDown();
            }
        }
    }

    public bool CanFire()
    {
        return _canFire;
    }

    public bool StartedAiming()
    {
        return _aimingNow;
    }
}
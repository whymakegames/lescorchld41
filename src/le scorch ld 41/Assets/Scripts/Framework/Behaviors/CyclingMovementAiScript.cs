﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public class CyclingMovementAiScript : MonoBehaviour
{
    public Transform target;
    public Camera camera;
    private MovementControllerScript _movement;

    public float yOne;
    public float durationOne;
    public float yTwo;
    public float durationTwo;
    private Timer _one;
    private Timer _two;

    void Start()
    {
        _movement = GetComponent<MovementControllerScript>();
        _one = new Timer(durationOne);
        _two = new Timer(durationTwo);
    }

    void Update()
    {
        if (!_one.TimeUp(Time.deltaTime))
        {
            _movement.SetInput(0, yOne);
        }

        if (_one.TimeUp(Time.deltaTime))
        {
            _two.Reset();
        }

        if (!_two.TimeUp(Time.deltaTime))
        {
            _movement.SetInput(0, yTwo);
        }

        if (_two.TimeUp(Time.deltaTime))
        {
            _one.Reset();
        }
    }
}
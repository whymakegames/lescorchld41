﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Platter : MonoBehaviour
{
    class AddedFood
    {
        public Vector3 offset;
        public LoadableFoodScript food;
    }

    public string recipeName;
    public Transform resultProto;
    public Transform recipePrepAnchor;
    public float prepSpacing = 0.15f;
    public Transform recipe;

    private Vector3 nextRecipePrepAnchor;
    private Dictionary<Ingredients, MealRecipePart> _recipeLookup;
    private List<AddedFood> _currentIngredients;
    private Vector3 _directionAnchorOffset;
    private AudioClip[] _plateNoises;

    void Start()
    {
        var recipeInstance = Instantiate(recipe);
        var recipes = recipeInstance.GetComponentsInChildren<MealRecipePart>();
        _recipeLookup = recipes.ToDictionary(p => p.ingredient, p => p);
        _currentIngredients = new List<AddedFood>();
        _directionAnchorOffset = new Vector3(0, prepSpacing, -0.2f);
        Debug.Log("OFFSET: " + _directionAnchorOffset);
        Debug.Log(recipePrepAnchor.position);

        TryInitPlateNoises();
    }

    void TryInitPlateNoises()
    {
        _plateNoises = _plateNoises ?? GameController.Instance.GetAudioClipsStartingWith("glock");
    }

    void AddIngredient(LoadableFoodScript food)
    {
        GameController.Instance.RandomSound(transform.position, _plateNoises);

        var added = new AddedFood
        {
            food = food,
            offset = _directionAnchorOffset * (_currentIngredients.Count + 1)
        };

        _currentIngredients.Add(added);

        food.transform.position = new Vector3(food.transform.position.x, food.transform.position.y, recipePrepAnchor.position.z + added.offset.z);

        food.SetMealPrepping();
    }

    LoadableFoodScript CompleteRecipe()
    {
        var middleFood = _currentIngredients[_currentIngredients.Count / 2];

        var result = Instantiate(resultProto, middleFood.food.transform.position, transform.rotation);

        var foodItem = result.GetComponent<LoadableFoodScript>();

        foreach (var food in _currentIngredients)
        {
            foodItem.AddChild(food.food);
        }

        _currentIngredients.Clear();

        return foodItem;
    }

    void Update()
    {
        TryInitPlateNoises();

        foreach (var food in _currentIngredients)
        {
            var target = recipePrepAnchor.position + food.offset;

            var lerpFactor = Time.deltaTime * 8;

            food.food.transform.position = Vector3.Lerp(food.food.transform.position, target, lerpFactor);
            food.food.transform.rotation = Quaternion.Lerp(food.food.transform.rotation, transform.rotation, lerpFactor);
        }
    }

    public RecipeScoreResult GetScore()
    {
        var extraIngredients = 0;
        var badPositions = 0;
        var allIngredients = _recipeLookup.Keys.ToArray();
        var containedIngredients = new List<Ingredients>();

        for (var i = 0; i < _currentIngredients.Count; i++)
        {
            var added = _currentIngredients[i];

            containedIngredients.Add(added.food.ingredient);

            MealRecipePart matchedIngredient;
            if (_recipeLookup.TryGetValue(added.food.ingredient, out matchedIngredient))
            {
                // was sequence wrong?
                if (!matchedIngredient.validPositions.Contains(i))
                {
                    badPositions++;
                }
            }
            else
            {
                extraIngredients++;
            }
        }

        return new RecipeScoreResult
        {
            Name = recipeName,
            ExtraIngredients = extraIngredients > 0,
            RightSequence = badPositions == 0,
            AllIngredients = allIngredients.All(a => containedIngredients.Contains(a))
        };
    }

    void OnCollisionEnter2D(Collision2D coll)
    {
        var food = coll.gameObject.GetComponent<LoadableFoodScript>();
        if (food != null && food.ingredient != Ingredients.CompletedRecipe)
        {
            AddIngredient(food);
            return;
        }

        var player = coll.gameObject.GetComponent<PlayerScript>();
        // can only pick up final meal if more than one ingredient
        if (player != null && _currentIngredients.Count > 1)
        {
            GameController.Instance.SetEndGameReached();

            var score = GetScore();
            GameController.ReportRecipeScore(score);

            var completed = CompleteRecipe();
            player.TryLoadFood(completed);

            // disable collider for this, now its permanently off
            gameObject.GetComponent<Collider2D>().enabled = false;
        }
    }
}

public class RecipeScoreResult
{
    public bool ExtraIngredients;
    public bool AllIngredients;
    public bool RightSequence;
    public string Name;
}
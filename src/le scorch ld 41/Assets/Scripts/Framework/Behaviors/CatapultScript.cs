﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CatapultScript : MonoBehaviour
{
    private string _animator_foodLoaded = "FoodLoaded";
    private string _animator_aimRelease = "AimReleased";
    private string _animator_aiming = "Aiming";

    public AudioClip fireSound;
    public AudioClip misfireSound;
    public AudioClip loadCompleteSound;

    public Transform aimControl;
    public Transform aimToward;
    public Transform foodAnchor;
    private LoadableFoodScript _loadedFood;
    private Animator _animator;
    private bool _startedAiming;
    private CannonScript _cannon;
    private string _lastTrigger;
    public float maxFirePowerTime = 3;
    public float maxFirePower = 150;
    private Timer _fullPower;
    public float upAimThreshold = 45;
    public float downAimThreshold = -45;
    float _aimDegrees = 1;
    private bool _fired;
    private float _firePower;
    private bool _hasFood;
    private SoundPlayerScript _sound;

    private int _fires;
    private Vector3 _fireStartPoint;
    private bool _loadComplete;
    private float _lastFireTime;

    public bool HasFood
    {
        get { return _hasFood; }
    }

    // Use this for initialization
    void Start()
    {
        _animator = GetComponent<Animator>();
        _fullPower = new Timer(maxFirePowerTime);
        _fullPower.Reset();
        aimToward.GetComponent<Renderer>().enabled = false;
        _sound = GetComponent<SoundPlayerScript>();
    }

    void Animate(string state)
    {
        if (_lastTrigger != null)
        {
            _animator.ResetTrigger(_lastTrigger);
        }

        _lastTrigger = state;
        _animator.SetTrigger(state);
    }

    public void Load(LoadableFoodScript food)
    {
        _loadComplete = false;
        _loadedFood = food;

        _loadedFood.SetIsLoaded();

        Animate(_animator_foodLoaded);
        _hasFood = true;
    }

    void Update()
    {
        if (_loadedFood != null)
        {
            if (_startedAiming)
            {
                if (_fired && Time.time - _lastFireTime > 2)
                {
                    Debug.Log("REDUNDANCY TRIGGERING FOOD FIRE BECAUSE CODES BROKEN");
                    FireFood();
                    return;
                }

                var percent = _fullPower.GetPercentElapsed();

                GameController.Instance.SetPowerScale(percent);

                _fullPower.TimeUp(Time.deltaTime);
                _firePower = percent * maxFirePower;
                aimToward.localScale = new Vector3(3, 3, 1) * percent;
            }

            var offset = -Vector3.ClampMagnitude(foodAnchor.transform.right, 0.1f);

            _loadedFood.transform.position = foodAnchor.transform.position + offset;

            Tuffy.LookAt2dRight(aimToward.transform.position, _loadedFood.transform);

            Debug.DrawLine(aimToward.transform.position, _loadedFood.transform.position, Color.cyan);
        }
    }

    public void SetLoadComplete()
    {
        _sound.Play(loadCompleteSound);
        _loadComplete = true;
    }

    public bool LoadComplete()
    {
        return _loadComplete;
    }

    public void Fire()
    {
        Debug.Log("FIRE!");

        _loadComplete = false;

        if (_loadedFood == null)
        {
            return;
        }

        aimToward.GetComponent<Renderer>().enabled = false;

        Animate(_animator_aimRelease);
        _fired = true;
        _lastFireTime = Time.time;

        _fireStartPoint = foodAnchor.transform.position;
        Debug.DrawLine(_fireStartPoint, foodAnchor.transform.position + foodAnchor.transform.forward, Color.yellow, 5);
    }

    public void RedundancyFireFood()
    {
        // just in case we end up back in the idle state on catapult and still have food?
        // FireFood();
    }

    public void FireFood()
    {
        _sound.Play(fireSound);

        if (_loadedFood != null)
        {
            Debug.Log("Fire food! " + _fires++);
            _loadedFood.Release(_firePower);
            _fullPower.Reset();
        }

        _loadComplete = false;
        _hasFood = false;
        _loadedFood = null;
        _loadComplete = false;
        _fired = false;
        _startedAiming = false;
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag == "ReleaseTrigger" && _fired)
        {
            FireFood();
            _hasFood = false;
        }
    }

    public bool StartedAiming()
    {
        return _startedAiming;
    }

    public void AimUp()
    {
        aimControl.transform.Rotate(Vector3.forward, _aimDegrees);
        ClampZ(upAimThreshold);
    }

    public void AimDown()
    {
        aimControl.transform.Rotate(Vector3.back, _aimDegrees);
        ClampZ(downAimThreshold);
    }

    void ClampZ(float aimThreshold)
    {
        Tuffy.ClampZEuler(aimControl.transform, aimThreshold);
    }

    public void UpdateAim(Vector2 input)
    {
        Debug.Log("AIM!");
        if (!_startedAiming)
        {
            _fired = false;
            _startedAiming = true;
            foreach (var state in new[] {_animator_foodLoaded, _animator_aimRelease, _animator_aiming})
            {
                _animator.ResetTrigger(state);
            }
        }
        
        aimToward.GetComponent<Renderer>().enabled = true;

        Animate(_animator_aiming);

        var dir = Tuffy.GetNegPos(input.y, 0.1f);

        if (dir != 0)
        {
            if (dir == 1)
            {
                AimUp();
            }
            else
            {
                AimDown();
            }
        }
    }
}
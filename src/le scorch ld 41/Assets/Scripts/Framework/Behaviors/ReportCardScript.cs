﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ReportCardScript : MonoBehaviour
{
    private SpriteRenderer _sprite;
    private TextMesh _text;
    private bool _visible;

    void Start()
    {
        _sprite = GetComponentInChildren<SpriteRenderer>();
        _sprite.color = new Color(1, 1, 1, 0);
        _text = GetComponentInChildren<TextMesh>();
        _text.GetComponent<Renderer>().sortingLayerID = _sprite.sortingLayerID;
    }

    public void Show(string report)
    {
        _visible = true;
        _text.text = report;
        _text.GetComponent<MeshRenderer>().enabled = true;
    }

    void Update()
    {
        if (_visible)
        {
            _sprite.color = Color.Lerp(_sprite.color, Color.white, Time.deltaTime);
            if (Input.anyKey)
            {
                SceneManager.LoadScene(0);
            }
        }
    }
}

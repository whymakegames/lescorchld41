﻿using UnityEngine;

public class AwareAnchorScript : MonoBehaviour
{
    private GroundCheckerScript _grounder;

    public void NotifyAnchor(GroundCheckerScript grounder)
    {
        _grounder = grounder;
    }

    public void NotifyAnchorBroken()
    {
        _grounder = null;
    }

    public GroundCheckerScript GetAnchoredObject()
    {
        return _grounder;
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

public enum Ingredients
{
    Invalid,

    Lettuce,
    SliceLettuce,

    Cow,
    RawBurger,
    CookedBurger,

    TopBun,
    BottomBun,

    Tomato,
    SliceTomato,

    CompletedRecipe
}
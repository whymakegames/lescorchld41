﻿using System;
using System.Diagnostics;

/// <summary>
/// utility functions to do stuff i do know how to do
/// </summary>
public static class Easy
{
    private static Random _random = new Random();

    public static T Random<T>(T[] items)
    {
        if (items.Length == 0)
        {
            return default(T);
        }

        return items[_random.Next(items.Length)];
    }

    public static bool Chance(float f)
    {
        return UnityEngine.Random.value < f;
    }
}